package eu.polyfox.pipbot.invites.command;

import eu.polyfox.pipbot.command.CommandHandler;
import eu.polyfox.pipbot.command.CommandInvocationContext;
import eu.polyfox.pipbot.invites.InvitesPlugins;
import eu.polyfox.pipbot.permission.PermissionRequirement;
import eu.polyfox.pipbot.util.EmbedFormatter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

/**
 * Handles the 'invite' command.
 * @author tr808axm
 */
public class InviteCommandHandler extends CommandHandler {
    private final InvitesPlugins plugin;

    public InviteCommandHandler(InvitesPlugins plugin) {
        super(plugin, new String[]{"invite"}, "Add this bot to your server.", "",
                new PermissionRequirement("command.invite", false, true));
        this.plugin = plugin;
    }

    @Override
    protected Message execute(CommandInvocationContext context) {
        EmbedBuilder response = context.response("Add this bot to your server", "To add this bot to another Discord server, you " +
                "need to be an administrator on that server.", EmbedFormatter.INFO)
                .addField("Invite Link", "[Click here](" + plugin.getInviteLink() + ")", true);
        context.respond(response.build());
        return null;
    }
}
