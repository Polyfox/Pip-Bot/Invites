package eu.polyfox.pipbot.invites;

import eu.polyfox.pipbot.invites.command.InviteCommandHandler;
import eu.polyfox.pipbot.plugin.Plugin;
import lombok.Getter;

import java.util.logging.Logger;

/**
 * "Add the bot to your server".
 * @author tr808axm
 */
@Getter
public class InvitesPlugins extends Plugin {
    private final Logger logger = Logger.getLogger("Giveaways");

    @Override
    public void enable() {
        getPipBot().getCommandManager().registerCommandHandler(new InviteCommandHandler(this));
        getLogger().info("Enabled Invites.");
    }

    @Override
    public void disable() {
        getLogger().info("Disabled Invites.");
    }

    @Override
    public String getName() {
        return "Giveaways";
    }

    @Override
    public String getAuthorName() {
        return "traxam#7012";
    }

    @Override
    public long getAuthorId() {
        return 137263174675070976L;
    }

    @Override
    public String getWebsite() {
        return "https://gitlab.com/Polyfox/Pip-Bot/Giveaways";
    }

    public String getInviteLink() {
        return "https://discordapp.com/oauth2/authorize?client_id=" + getPipBot().getJda().getSelfUser().getId() + "&permissions=-1&scope=bot";
    }
}
